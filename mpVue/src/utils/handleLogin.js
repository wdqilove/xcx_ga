import wx from './wx'
import request from './request'
import store from '@/store'
// 开始login
export function login (callback) {
  wx.showLoading()
  wx.login({
    success (res) {
      if (res.code) {
        console.log(res)
        // 登录成功，获取用户信息
        getUserInfo(res.code, callback)
      } else {
        // 否则弹窗显示，showToast需要封装
        showToast()
      }
    },
    fail () {
      showToast()
    }
  })
}
function setStorage (key, value) {
  return new Promise((resolve, reject) => {
    wx.setStorage({ key: key, data: value, success: resolve, fail: reject })
  })
}
function getStorageSync (key) {
  return wx.getStorageSync(key)
}
function getStorage (key) {
  return new Promise((resolve, reject) => {
    wx.getStorage({ key: key, success: resolve, fail: reject })
  })
}
// 获取用户信息
function getUserInfo (code, callback) {
  wx.getUserInfo({
    // 获取成功，全局存储用户信息，开发者服务器登录
    success (res) {
      // 全局存储用户信息
      store.commit('storeUpdateWxUser', res.userInfo)
      postLogin(code, res.iv, res.signature, res.rawData, res.encryptedData, callback)
    },
    // 获取失败，弹窗提示一键登录
    fail () {
      wx.hideLoading()
      // 获取用户信息失败，清楚全局存储的登录状态，弹窗提示一键登录
      // 使用token管理登录态的，清楚存储全局的token
      // 使用cookie管理登录态的，可以清楚全局登录状态管理的变量
      store.commit('storeUpdateToken', '')
      // 获取不到用户信息，说明用户没有授权或者取消授权。弹窗提示一键登录，后续会讲
      showLoginModal()
    }
  })
}

// 开发者服务端登录
function postLogin (code, iv, signature, rawData, encryptedData, callback) {
  let params = {
    sid: store.state.sid,
    code: code,
    iv: iv,
    signature: signature,
    rawData: rawData,
    encryptedData: encryptedData,
    share: store.state.sharetoken
  }
  request.post('https://guo-a.com/mapi/user/get3rdSession', params, ).then((res) => {
    console.log(res)
    if (res.code == 200) {
      wx.hideLoading()
      // 登录成功，
      // 使用token管理登录态的，存储全局token，用于当做登录态判断，
      // 使用cookie管理登录态的，可以存任意变量当做已登录状态
      // setStorage('storeUpdateToken', res.data.token)
      store.commit('storeUpdateToken', res.data.session3rd)
      callback && callback()
    } else {
      showToast()
    }
  }).catch((err) => {
    showToast()
  })
}

// 显示toast弹窗
export function showToast (content = '登录失败，请稍后再试') {
  wx.showToast({
    title: content,
    icon: 'none'
  })
}
// 判断是否登录
export function isLogin (callback) {
  let token = store.state.Token
  if (token) {
    request.post('https://guo-a.com/mapi/api/isLogin?token=' + token).then((res) => {
      if(res.code !== 200){
        showLoginModal()
      }else{
        callback && callback()
      }
    })
    // 如果有全局存储的登录态，暂时认为他是登录状态
    // callback && callback()
  } else {
    // 如果没有登录态，弹窗提示一键登录
    showLoginModal()
  }
}

// 接口调用失败处理，
export function handleError (res, callback) {
  // 规定-3041和-3042分别代表未登录和登录态失效
  if (res.code == 40001) {
    // 弹窗提示一键登录
    showLoginModal()
  } else if (res.msg) {
    // 弹窗显示错误信息
    showToast(res.msg)
  }
}
// 显示一键登录的弹窗
function showLoginModal () {
  wx.showModal({
    title: '提示',
    content: '你还未登录，登录后可获得完整体验 ',
    confirmText: '一键登录',
    success (res) {
      // 点击一键登录，去授权页面
      if (res.confirm) {
        wx.navigateTo({
          url: '/pages/login/main',
        })
      }
    }
  })
}