import wx from './wx'
import requst from './request'
let util = {}

// 请求封装
util.ajax = requst

util.deepClone = (obj) => {
  let proto = Object.getPrototypeOf(obj)
  return Object.assign({}, Object.create(proto), obj)
}

util.changeTwoDecimal = (x) => {
  let fx = parseFloat(x)
  if (isNaN(fx)) {
    console.log('function:changeTwoDecimal->parameter error')
    return false
  }
  fx = Math.round(x * 100) / 100
  let sx = fx.toString()
  let posDecimal = sx.indexOf('.')
  if (posDecimal < 0) {
    posDecimal = sx.length
    sx += '.'
  }
  while (sx.length <= posDecimal + 2) {
    sx += '0'
  }
  return sx
}
// 操作成功提示
util.successToast = (msg, callback) => {
  wx.showToast({
    title: msg,
    icon: 'success',
    success: callback
  })
}

// 操作失败提示
util.errorToast = (msg, callback) => {
  wx.showToast({
    title: msg,
    icon: 'none',
    success: callback
  })
}

export default util
