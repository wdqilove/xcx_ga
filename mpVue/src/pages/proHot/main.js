import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()
export default {
  config: {
    navigationBarBackgroundColor: '#ef5c53',
    navigationBarTitleText: '热销排行 '
  }
}
