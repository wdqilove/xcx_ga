import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()
export default {
  config: {
    navigationBarBackgroundColor: '#ff3a35',
    navigationBarTitleText: '选择优惠券 '
  }
}
