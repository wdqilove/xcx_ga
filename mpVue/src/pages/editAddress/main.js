import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()
export default {
  config: {
    navigationBarTextStyle: 'black',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: '编辑地址 '
  }
}
