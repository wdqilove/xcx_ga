import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()
export default {
  config: {
    navigationBarBackgroundColor: '#434343',
    navigationBarTitleText: '我的团队 '
  }
}
