import Vue from 'vue'
import App from './App'
import '../static/weui/weui.css'
import util from './utils/util'
import store from '@/store'

Vue.config.productionTip = false
App.mpType = 'app'

Vue.prototype.$_util = util
Vue.prototype.$store = store

const app = new Vue(App)
app.$mount()

export default {
  // 这个字段走 app.json
  config: {
    // 页面前带有 ^ 符号的，会被编译成首页，其他页面可以选填，我们会自动把 webpack entry 里面的入口页面加进去
    pages: [
      '^pages/home/main', // 首页
      // '^pages/test/main', // 测试
      'pages/share/main',//分享有礼
      'pages/mycollect/main',//我的收藏
      'pages/shopgroup/main',//团购详情
      'pages/pay/main',//支付
      'pages/order/main',//订单确认
      'pages/login/main',//授权页面
      'pages/category/main', // 分类
      'pages/distribution/main', // 分销中心
      'pages/teams/main', // 我的团队
      'pages/commission/main', // 佣金明细
      'pages/consumer/main', // 一级消费者
      'pages/distributionApply/main', // 分销申请
      'pages/withDraw/main', // 提现
      'pages/recharge/main', // 充值
      'pages/editAddress/main', // 编辑地址
      'pages/order/main', // 提交订单
      'pages/orderDetail/main', // 订单详情
      'pages/shopCart/main', // 购物车
      'pages/tickets/main', // 领券中心
      'pages/myTickets/main', // 我的优惠券
      'pages/shopDetail/main', // 商品详情
      'pages/comments/main',//商品评价
      'pages/rule/main', // 规则
      'pages/proList/main', // 商品列表
      'pages/proHot/main', // 热销
      'pages/proSpike/main', // 秒杀
      'pages/proGroup/main', // 好友拼团
      'pages/proFresh/main', // 新鲜预售
      'pages/proHalf/main',// 第二盒半价
      'pages/myOrder/main',// 我的订单
      'pages/proSpike/main',//限时秒杀
      'pages/SeckillDetail/main',//限时秒杀详情页
      'pages/confirmseckill/main',//限时秒杀确认
      'pages/nearbyShop/main',//附近门店
      // 'pages/nearbyShopDetail/main',//附近门店详情
      'pages/about/main',//关于我们
      'pages/orderComment/main',//商品订单评价
      'pages/nearbyShopDetail/main',//附近门店详情
      'pages/storeorder/main',//门店商品确认订单
      'pages/storeorderdetail/main',//门店订单详情
      'pages/storeorderlist/main',//门店订单列表
    ],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#ff3a35',
      navigationBarTitleText: '菓啊 ',
      navigationBarTextStyle: 'white',
      // enablePullDownRefresh:true,
    },
    tabBar: {
      color: '#626567',
      selectedColor: '#ff3a35',
      backgroundColor: '#FBFBFB',
      borderStyle: 'white',
      list: [
        {
          pagePath: 'pages/home/main',
          text: '首页',
          iconPath: 'static/images/icon/tab_icon1.png',
          selectedIconPath: 'static/images/icon/tab_iconh1.png'
        },
        {
          pagePath: 'pages/category/main',
          text: '分类',
          iconPath: 'static/images/icon/tab_icon2.png',
          selectedIconPath: 'static/images/icon/tab_iconh2.png'
        },
        {
          pagePath: 'pages/shopCart/main',
          text: '购物车',
          iconPath: 'static/images/icon/tab_icon3.png',
          selectedIconPath: 'static/images/icon/tab_iconh3.png'
        },
        {
          pagePath: 'pages/my/main',
          text: '个人中心',
          iconPath: 'static/images/icon/tab_icon5.png',
          selectedIconPath: 'static/images/icon/tab_iconh5.png'
        }
      ]
    }
  }
}
