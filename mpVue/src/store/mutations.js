import{
	UP_USER,
	UP_TOKEN,
	UP_SID,
	UP_SHARE
} from './mutation-types.js'

export default {
	[UP_USER] (state, v) {
		state.WxUser = v
	},
	[UP_TOKEN] (state, v) {
		state.Token = v
	},
	[UP_SID] (state, v) {
		state.sid = v
	},
	[UP_SHARE] (state, v) {
		state.sharetoken = v
	}
}